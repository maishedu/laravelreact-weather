/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
require('./bootstrap');
import React from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Switch,
    withRouter,
    Route,
    Link
} from "react-router-dom";
import ("./../css/app.css");
import ("weather-icons/css/weather-icons.css");
import Home from "./components/Home";

function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Home}></Route>
            </Switch>
        </Router>
    );
}

export default App;
// DOM element
if (document.getElementById('user')) {
    ReactDOM.render(<App />, document.getElementById('user'));
}


/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */



