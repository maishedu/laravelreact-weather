import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import axios from "axios";
const API_Key = "09b40dca05e02e51f08cd38801100ea6";
const weather_icon = {
    Thunderstorm : "wi-night-alt-thunderstorm",
    Drizzle : "wi-sleet",
    Rain : "wi-day-storm-showers",
    Snow : "wi-snow",
    Atmosphere: "wi-fog",
    Clear: "wi-day-sunny",
    Clouds: "wi-day-fog"

}

class Home extends Component{
    constructor() {
        super();
        this.state = {
            city : '',
            country : '',
            icon_id: '',
            icon: '',
            main:'',
            celcius:'',
            temp_max:'',
            temp_min:'',
            description:'',
            city_call:'',
            country_call:'',
            error: false
        };
    }
    componentDidMount() {
    }

    calcelcius(temp){
        let cell = Math.floor(temp -273.15);
        return cell;
    }
    getWeather = async()=>{
        const api_call = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${this.state.city},${this.state.country}&appid=${API_Key}`);
        const res = await api_call.json();
        console.log(res);
        this.setState({
            celcius : this.calcelcius(res.main.temp),
            temp_max:this.calcelcius(res.main.temp_max),
            temp_min:this.calcelcius(res.main.temp_min),
            description: res.weather[0].description,
            icon_id : res.weather[0].id,
            city_call:res.name,
            country_call:res.sys.country
        });
        this.get_weathericon(this.state.icon_id);
    }
    minmaxTemp = (min,max)=>{
        return(
             <h3>
                <span className="px-4">{min}&deg;</span>
                <span className="px-4">{max}&deg;</span>
             </h3>
        );
    }
    get_weathericon(rangeid){
        switch (true){
            case rangeid>=200 && rangeid<=232:
                this.setState({icon:weather_icon.Thunderstorm});
                break;
            case rangeid>=300 && rangeid<=321:
                this.setState({icon:weather_icon.Drizzle});
                break;
            case rangeid>=500 && rangeid<=531:
                this.setState({icon:weather_icon.Rain});
                break;
            case rangeid>=600 && rangeid<=621:
                this.setState({icon:weather_icon.Snow});
                break;
            case rangeid>=701 && rangeid<=781:
                this.setState({icon:weather_icon.Atmosphere});
                break;
            case rangeid=800:
                this.setState({icon:weather_icon.Clear});
                break;
            case rangeid>=801 && rangeid<=804:
                this.setState({icon:weather_icon.Clouds});
                break;
            default:this.setState({icon:weather_icon.Clouds});
        }
    }
    handleInput =(e)=>{
        this.setState({
            [e.target.name] : e.target.value
        });
    }
    call_api = (e)=>{
        e.preventDefault();
        this.getWeather();
    }
    render() {
        return(
            <div className="container pt-3">
                <div className="row">
                    <div className="col-md-12 justify-content-center text-center">
                        <h1>Weather App</h1>
                        <div className="row pb-2">
                            <div className="col-md-6">
                                <form onSubmit={this.call_api}>
                                    <div className="input-group">
                                        <input type="text" name="city" className="form-control" onChange={this.handleInput} value={this.state.city} placeholder="City"/>
                                        <span className="input-group-addon">-</span>
                                        <input type="text" name="country" className="form-control" onChange={this.handleInput} value={this.state.country} placeholder="Country"/>
                                        <button type="submit" className="btn btn-warning">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="card">
                            <h1>{this.state.city_call},{this.state.country_call}</h1>
                            <h5 className="py-5">
                                <i className={`wi ${this.state.icon} display-1`}></i>
                            </h5>
                            <h1 className="py-2">{this.state.celcius}&deg;</h1>
                            {this.minmaxTemp(this.state.temp_max,this.state.temp_min)}
                            <h4 className="py-3">{this.state.description}</h4>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Home;


